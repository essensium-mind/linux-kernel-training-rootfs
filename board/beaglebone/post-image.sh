#!/bin/bash

set -x

rm /srv/tftp/linux-kernel-labs/{zImage,am335x-boneblack.dtb}
cp "${BASE_DIR}"/images/{zImage,am335x-boneblack.dtb} /srv/tftp/linux-kernel-labs/

