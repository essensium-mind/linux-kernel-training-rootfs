#!/bin/bash
ADDR=192.168.0.100

IMAGEDIR="$(dirname $0)/../output/beaglebone/images"
echo $IMAGEDIR

PASS="root"
SSH="sshpass -p $PASS ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"
SCP="sshpass -p $PASS scp -O -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

$SSH root@$ADDR 'mount /dev/mmcblk1p1 /mnt'
$SCP $IMAGEDIR/u-boot.img root@$ADDR:/mnt/u-boot.img
$SCP $IMAGEDIR/am335x-boneblack.dtb root@$ADDR:/mnt/am335x-boneblack.dtb
$SCP $IMAGEDIR/uboot-env.bin root@$ADDR:/mnt/uboot-env.bin
$SCP $IMAGEDIR/zImage root@$ADDR:/mnt/zImage
$SSH root@$ADDR 'umount /mnt'
$SSH root@$ADDR 'reboot'
