# linux-kernel-training-rootfs scripts

## Flashing without SD card with snagboot

If you have a working uboot & kernel don't bother doing all of this. Just mount
the boot partition and replace the `u-boot.img` with a new one.

If your device is bricked or you want to flash the eMMC from your computer
follow the next steps.

First, install the snagboot udev rules.

```shell
sudo cp 50-snagboot.rules /etc/udev/rules.d/
sudo udevadm control --reload-rules
sudo udevadm trigger
```

Be sure to set the board in [recovery mode](https://github.com/bootlin/snagboot/blob/main/docs/board_setup.md#the-special-case-of-ti-am335x-devices).

Basically you have to remove the SD Card and hold the S2 button when powering
up the board. You will see `C` printed on the serial terminal.

Run the `am335x_usb_setup.sh` script.

```shell
sudo ./scripts/am335x_usb_setup.sh
```

Flash the U-Boot present in `output/beaglebone/images/u-boot.img`. This img is
configured to fetch the kernel and dtb image from a tftp server running on
`192.168.0.1`.

```shell
snagrecover -s am3358 -f scripts/fw.yaml
```

### Optionnal: Flash the eMMC with the SD card image

If you want to load your own environment variable for U-Boot.

Make sure to stop the autoboot to enter U-Boot shell to enter in fastboot
mode to flash the SD Card. Enter the following commands in the U-Boot shell.

```shell
env default -a
unbind ethernet 1
fastboot usb 0
```

And then on the host terminal run the following command to flash the eMMC

```shell
snagflash -P fastboot -p 0451:d022 -f oem_format -f download:output/beaglebone/images/sdcard.img -f flash:1:0
```

## Network configuration for USB gadget

If you are using `systemd-networkd` copy the `24-gadget.network` file in
`/etc/systemd/network/`. When it detects the driver of the gadget is registered
it will set you computer static ip address to `192.168.0.1`.

```shell
sudo cp 24-gadget.network /etc/systemd/network/
sudo systemctl restart systemd-networkd
```
