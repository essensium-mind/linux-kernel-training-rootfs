# Linux Kernel Training Image

This repository contains the buildroot configuration to generate a
pre-configured `u-boot` image to run the kernel training.

- If you want put the `linux` source in the `src/linux` directory
  to profit from the cross-compilation handled by buildroot.
- U-Boot & Kernel image preconfigured to support USB Gadget.
  - Device ip is `192.168.0.100` and host `192.168.0.1` check
    `scripts/24-gadget.network` for a `systemd-networkd` configuration
    sample.
- U-Boot download the `zImage` & `am335x-boneblack.dtb`.
  - Files should be located at `/srv/tftp/linux-kernel-labs`.
- U-Boot env is preset to boot kernel with NFS args.
  - NFS should be located at `/srv/nfs/linux-kernel-labs/`.

Also make sure to configure `nfs` with the following line.

```
/srv/nfs/linux-kernel-labs/ 192.168.0.100(rw,no_root_squash,no_subtree_check)
```

And run `sudo exportfs -arv`.

To flash the device information check the `scripts/README.md` file.
